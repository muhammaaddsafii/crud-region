<?php

namespace App\Http\Controllers;

use App\District;
use App\Province;
use App\Subdistrict;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class SubdistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subdistricts = Subdistrict::get();
        return view('subdistrict.index', ['subdistricts' => $subdistricts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::get();
        $districts = District::get();
        return view('subdistrict.create', [
            'provinces' => $provinces,
            'districts' => $districts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attr = $request->validate([
            'name' => 'required',
        ]);

        $attr['slug'] = Str::slug($request->name);
        $attr['province_id'] = request('province');
        $attr['district_id'] = request('district');


        Subdistrict::create($attr);

        // session()->flash('success', 'The post has created');
        // session()->flash('error', 'The post has failed to create');

        return redirect()->to('/sub-district');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Subdistrict $subdistricts)
    {
        return view('subdistrict.edit', [
            'subdistricts' => $subdistricts,
            'districts' => District::get(),
            'provinces' => Province::get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subdistrict $subdistricts)
    {
        $attr = request()->validate([
            'name' => 'required'
        ]);

        $attr['province_id'] = request('province');
        $attr['subdistrict_id'] = request('district');

        $subdistricts->update($attr);
        return redirect()->to('sub-district');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subdistrict $subdistricts)
    {
        $subdistricts->villages()->delete();
        $subdistricts->delete();
        return redirect('sub-district');
    }
}
