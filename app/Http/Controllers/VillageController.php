<?php

namespace App\Http\Controllers;

use App\District;
use App\Province;
use App\Subdistrict;
use App\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class VillageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $villages = Village::get();
        return view('village.index', ['villages' => $villages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::get();
        $districts = District::get();
        $subdistricts = Subdistrict::get();
        return view('village.create', [
            'provinces' => $provinces,
            'districts' => $districts,
            'subdistricts' => $subdistricts,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attr = $request->validate([
            'name' => 'required',
        ]);

        $attr['slug'] = Str::slug($request->name);
        $attr['province_id'] = request('province');
        $attr['district_id'] = request('district');
        $attr['subdistrict_id'] = request('subdistrict');


        Village::create($attr);

        // session()->flash('success', 'The post has created');
        // session()->flash('error', 'The post has failed to create');

        return redirect()->to('/village');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Village $villages)
    {
        return view('village.edit', [
            'villages' => $villages,
            'districts' => District::get(),
            'subdistricts' => Subdistrict::get(),
            'provinces' => Province::get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Village $villages)
    {
        $attr = request()->validate([
            'name' => 'required'
        ]);

        $attr['province_id'] = request('province');
        $attr['district_id'] = request('district');
        $attr['district_id'] = request('subdistrict');

        $villages->update($attr);
        return redirect()->to('village');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Village $villages)
    {
        $villages->delete();
        return redirect('village');
    }
}
