<?php

namespace App\Http\Controllers;

use App\District;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $districts = District::get();
        return view('district.index', ['districts' => $districts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::get();
        return view('district.create', ['provinces' => $provinces]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attr = $request->validate([
            'name' => 'required',
        ]);

        $attr['province_id'] = request('province');
        $attr['slug'] = Str::slug($request->name);

        District::create($attr);

        // session()->flash('success', 'The post has created');
        // session()->flash('error', 'The post has failed to create');

        return redirect()->to('/district');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(District $districts)
    {
        return view('district.edit', [
            'districts' => $districts,
            'provinces' => Province::get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, District $districts)
    {
        $attr = request()->validate([
            'name' => 'required'
        ]);

        $attr['province_id'] = request('province');

        $districts->update($attr);
        return redirect()->to('district');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $districts)
    {
        $districts->subdistricts()->delete();
        $districts->villages()->delete();
        $districts->delete();
        return redirect('district');
    }
}
