@extends('layout.master')

@section('content')
<div class="container">
    <div class="d-flex justify-content-between mt-4">
        <h4 class="text-secondary">Daftar Kabupaten di Indonesia</h4>
        <a href="/district/create" class="btn btn-primary rounded-pill mb-3">Tambah Data</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr class="text-center">
              <th scope="col">id</th>
              <th scope="col">Nama Kabupaten</th>
              <th scope="col">Nama Provinsi</th>
              <th scope="col">Jumlah Kecamatan</th>
              <th scope="col">Jumlah Desa</th>
              <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($districts as $district)
        <tr class="text-center">
            <th scope="row">{{ $district->id }}</th>
            <td>{{ $district->name }}</td>
            <td>{{ $district->province->name }}</td>
            <td>{{ $district->subdistricts->count() }}</td>
            <td>{{ $district->villages->count() }}</td>
            <td>
                <a href="/district/{{ $district->slug }}/edit" class="btn btn-sm btn-warning rounded-pill">Edit</a>
                <form action="/district/{{ $district->slug }}/delete" method="POST" class="d-inline">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger rounded-pill">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
