@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h4 class="text-secondary mt-3 mb-3">Menambahkan Data Kabupaten Baru</h4>
            <form action="/district/store" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name">Nama Kabupaten</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="province">Provinsi</label>
                    <select name="province" id="province" class="form-control">
                        <option value="" selected disabled>Pilih Provinsi :</option>
                        @foreach ($provinces as $province)
                        <option value="{{ $province->id }}">{{ $province->name }}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <button type="submit" class="btn btn-primary rounded-pill">Tambah</button>
            </form>
        </div>
    </div>
</div>
@endsection
