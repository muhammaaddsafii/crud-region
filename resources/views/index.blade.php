@extends('layout.master')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-6 mt-5">
            <div class="card text-center">
                <div class="card-title ">
                    Jumlah Provinsi : {{ $provinces->count() }}
                </div>
            </div>
        </div>
        <div class="col-6 mt-5">
            <div class="card text-center">
                <div class="card-title">
                    Jumlah Kabupaten : {{ $districts->count() }}
                </div>
            </div>
        </div>
        <div class="col-6 mt-5">
            <div class="card text-center">
                <div class="card-title">
                    Jumlah Kecamatan : {{ $subdistricts->count() }}
                </div>
            </div>
        </div>
        <div class="col-6 mt-5">
            <div class="card text-center">
                <div class="card-title">
                    Jumlah Desa : {{ $villages->count() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
