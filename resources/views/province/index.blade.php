@extends('layout.master')

@section('content')
<div class="container">
    <div class="d-flex justify-content-between mt-4">
        <h4 class="text-secondary">Daftar Provinsi di Indonesia</h4>
        <a href="/province/create" class="btn btn-primary rounded-pill mb-3">Tambah Data</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr class="text-center">
                <th scope="col">id</th>
                <th scope="col">Nama Provinsi</th>
                <th scope="col">Jumlah Kabupaten</th>
                <th scope="col">Jumlah Kecamatan</th>
                <th scope="col">Jumlah Desa</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($provinces as $province)
        <tr class="text-center">
            <th scope="row">{{ $province->id}}</th>
            <td>{{ $province->name }}</td>
            <td>{{ $province->districts->count() }}</td>
            <td>{{ $province->subdistricts->count() }}</td>
            <td>{{ $province->villages->count() }}</td>
            <td>
                <a href="/province/{{ $province->slug }}/edit" class="btn btn-sm btn-warning rounded-pill">Edit</a>
                <form method="POST" action="/province/{{ $province->slug }}/delete" class="d-inline">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger rounded-pill">Delete</button>
                </form>
            </td>
        @endforeach
        </tr>
        </tbody>
    </table>
</div>
@endsection
