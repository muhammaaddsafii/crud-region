@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h4 class="text-secondary mt-3 mb-3">Menambahkan Data Provinsi Baru</h4>
            <form action="/province/store" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name">Nama Provinsi</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>
                <br>
                <button type="submit" class="btn btn-primary rounded-pill">Tambah</button>
            </form>
        </div>
    </div>
</div>
@endsection
