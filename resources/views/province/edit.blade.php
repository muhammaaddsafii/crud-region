@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h4 class="text-secondary mt-3 mb-3">Memperbarui Data Provinsi</h4>
            <form action="/province/{{ $provinces->slug }}/edit" method="POST" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <div class="form-group">
                    <label for="name">Nama Provinsi</label>
                    <input type="text" name="name" value="{{ old('name') ?? $provinces->name }}" id="name" class="form-control">
                </div>
                <br>
                <button type="submit" class="btn btn-primary rounded-pill">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection
