@extends('layout.master')

@section('content')
<div class="container">
    <div class="d-flex justify-content-between mt-4">
        <h4 class="text-secondary">Daftar Kecamatan di Indonesia</h4>
        <a href="/sub-district/create" class="btn btn-primary rounded-pill mb-3">Tambah Data</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr class="text-center">
              <th scope="col">id</th>
              <th scope="col">Nama Kecamatan</th>
              <th scope="col">Nama Kabupaten</th>
              <th scope="col">nama Provinsi</th>
              <th scope="col">Jumlah Desa</th>
              <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($subdistricts as $subdistrict)
        <tr class="text-center">
            <th scope="row">{{ $subdistrict->id }}</th>
            <td>{{ $subdistrict->name }}</td>
            <td>{{ $subdistrict->district->name }}</td>
            <td>{{ $subdistrict->province->name }}</td>
            <td>{{ $subdistrict->villages->count() }}</td>
            <td>
                <a href="/sub-district/{{ $subdistrict->slug }}/edit" class="btn btn-sm btn-warning rounded-pill">Edit</a>
                <form action="/sub-district/{{ $subdistrict->slug }}/delete" class="d-inline" method="POST">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger rounded-pill">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
