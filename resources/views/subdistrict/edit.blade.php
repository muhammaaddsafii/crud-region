@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h4 class="text-secondary mt-3 mb-3">Edit Data Kecamatan</h4>
            <form action="/sub-district/{{$subdistricts->slug}}/edit" method="POST" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <div class="form-group">
                    <label for="name">Nama Kecamatan</label>
                    <input type="text" name="name" id="name" value="{{ old('name') ?? $subdistricts->name }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="district">Kabupaten</label>
                    <select name="district" id="district" class="form-control">
                        <option value="" selected disabled>Pilih Kabupaten :</option>
                        @foreach ($districts as $district)
                        <option {{ $district->id == $subdistricts->district_id ? 'selected' : '' }} value="{{ $district->id }}">{{ $district->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="province">Provinsi</label>
                    <select name="province" id="province" class="form-control">
                        <option value="" selected disabled>Pilih Provinsi :</option>
                        @foreach ($provinces as $province)
                        <option {{ $province->id == $subdistricts->province_id ? 'selected' : ''}} value="{{ $province->id }}">{{ $province->name }}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <button type="submit" class="btn btn-primary rounded-pill">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection
