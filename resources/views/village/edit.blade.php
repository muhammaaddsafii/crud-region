@extends('layout.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h4 class="text-secondary mt-3 mb-3">Edit Data Desa</h4>
            <form action="/village/{{ $villages->slug }}/edit" method="POST" enctype="multipart/form-data">
                @method('patch')
                @csrf
                <div class="form-group">
                    <label for="name">Nama Desa</label>
                    <input type="text" name="name" id="name" value="{{ old('name') ?? $villages->name}}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="subdistrict">Kecamatan</label>
                    <select name="subdistrict" id="subdistrict" class="form-control">
                        <option value="" selected disabled>Pilih Kecamatan :</option>
                        @foreach ($subdistricts as $subdistrict)
                        <option {{ $subdistrict->id == $villages->subdistrict_id ? 'selected' : '' }} value="{{ $subdistrict->id }}">{{ $subdistrict->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="district">Kabupaten</label>
                    <select name="district" id="district" class="form-control">
                        <option value="" selected disabled>Pilih Kabupaten :</option>
                        @foreach ($districts as $district)
                        <option {{ $district->id == $villages->district_id ? 'selected' : '' }} value="{{ $district->id }}">{{ $district->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="province">Provinsi</label>
                    <select name="province" id="province" class="form-control">
                        <option value="" selected disabled>Pilih Provinsi :</option>
                        @foreach ($provinces as $province)
                        <option {{ $province->id == $villages->province_id ? 'selected' : '' }} value="{{ $province->id }}">{{ $province->name }}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                <button type="submit" class="btn btn-primary rounded-pill">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection
