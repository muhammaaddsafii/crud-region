@extends('layout.master')

@section('content')
<div class="container">
    <div class="d-flex justify-content-between mt-4">
        <h4 class="text-secondary">Daftar Desa di Indonesia</h4>
        <a href="/village/create" class="btn btn-primary rounded-pill mb-3">Tambah Data</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr class="text-center">
              <th scope="col">id</th>
              <th scope="col">Nama Desa</th>
              <th scope="col">Nama Kecamatan</th>
              <th scope="col">Nama Kabupaten</th>
              <th scope="col">Nama Provinsi</th>
              <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($villages as $village)
        <tr class="text-center">
            <th scope="row">{{ $village->id }}</th>
            <td>{{ $village->name }}</td>
            <td>{{ $village->subdistrict->name }}</td>
            <td>{{ $village->district->name }}</td>
            <td>{{ $village->province->name }}</td>
            <td>
                <a href="/village/{{ $village->slug }}/edit" class="btn btn-sm btn-warning rounded-pill">Edit</a>
                <form action="/village/{{ $village->slug }}/delete" class="d-inline" method="POST">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger rounded-pill">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
