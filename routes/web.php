<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index');


// *** PROVINCE *** //
Route::get('/province', 'ProvinceController@index');
Route::get('/province/create', 'ProvinceController@create');
Route::post('/province/store', 'ProvinceController@store');
Route::get('/province/{provinces:slug}/edit', 'ProvinceController@edit');
Route::patch('/province/{provinces:slug}/edit', 'ProvinceController@update');
Route::delete('/province/{provinces:slug}/delete', 'ProvinceController@destroy');
// ***          *** //


// *** DISTRICT *** //
Route::get('/district', 'DistrictController@index');
Route::get('/district/create', 'DistrictController@create');
Route::post('/district/store', 'DistrictController@store');
Route::get('/district/{districts:slug}/edit', 'DistrictController@edit');
Route::patch('/district/{districts:slug}/edit', 'DistrictController@update');
Route::delete('/district/{districts:slug}/delete', 'DistrictController@destroy');
// ***          *** //


// *** SUB-DISTRICT *** //
Route::get('/sub-district', 'SubdistrictController@index');
Route::get('/sub-district/create', 'SubdistrictController@create');
Route::post('/sub-district/store', 'SubdistrictController@store');
Route::get('/sub-district/{subdistricts:slug}/edit', 'SubdistrictController@edit');
Route::patch('/sub-district/{subdistricts:slug}/edit', 'SubdistrictController@update');
Route::delete('/sub-district/{subdistricts:slug}/delete', 'SubdistrictController@destroy');
// ***          *** //


// *** VILLAGE *** //
Route::get('/village', 'VillageController@index');
Route::get('/village/create', 'VillageController@create');
Route::post('/village/store', 'VillageController@store');
Route::get('/village/{villages:slug}/edit', 'VillageController@edit');
Route::patch('/village/{villages:slug}/edit', 'VillageController@update');
Route::delete('/village/{villages:slug}/delete', 'VillageController@destroy');
// ***          *** //
